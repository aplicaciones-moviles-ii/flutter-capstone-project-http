import 'package:capstone_project/models/user.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;

class UserHttpRequest {
  Future<User> getRamdonUser() async {
    final http.Response response =
        await http.get(Uri.parse('https://randomuser.me/api'));
    return compute(userFromJson, response.body);
  }
}
