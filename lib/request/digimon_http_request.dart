import 'package:capstone_project/models/digimon.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;

class DigimonHttpRequest {
  Future<List<Digimon>> get getDigimons async {
    final http.Response response =
        //await http.get(Uri.parse('https://digimon-api.vercel.app/api/digimon'));
        await http.get(Uri.parse(
            'https://digimoncard.io/api-public/search.php?n=Agumon&desc'));
    return compute(digimonsFromJson, response.body);
  }

  Future<Digimon> getDigimon(String cardNumber) async {
    final http.Response response = await http.get(Uri.parse(
        'https://digimoncard.io/api-public/search.php?card=$cardNumber'));
    return compute(digimonFromJson, response.body);
  }

  Future<List<Digimon>> getDigimonCardSets(String cardSet) async {
    final http.Response response = await http.get(Uri.parse(
        'https://digimoncard.io/api-public/search.php?pack=$cardSet'));
    return compute(digimonsFromJson, response.body);
  }
}
