import 'package:capstone_project/models/user.dart';
import 'package:capstone_project/request/user_http_request.dart';
import 'package:capstone_project/widgets/getting_data_widget.dart';
import 'package:capstone_project/widgets/google_maps_widget.dart';
import 'package:flutter/material.dart';

class UserScreen extends StatefulWidget {
  const UserScreen({Key? key}) : super(key: key);

  @override
  State<UserScreen> createState() => _UserScreenState();
}

class _UserScreenState extends State<UserScreen> {
  late Future<User> futureUser;

  @override
  void initState() {
    super.initState();
    futureUser = UserHttpRequest().getRamdonUser();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<User>(
        future: futureUser,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const gettingDataWidget();
          }
          User? miUser = snapshot.data;
          // ignore: avoid_print
          print(miUser!.results[0].picture.medium);
          return MaterialApp(
            home: Scaffold(
              backgroundColor: Colors.blueGrey,
              body: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  CircleAvatar(
                    radius: 35.0,
                    backgroundImage:
                        NetworkImage(miUser.results[0].picture.large),
                  ),
                  Text(
                    miUser.results[0].name.first +
                        " " +
                        miUser.results[0].name.last,
                    style: const TextStyle(
                      fontSize: 20.0,
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      letterSpacing: -1.0,
                    ),
                  ),
                  Text(
                    miUser.results[0].email,
                    style: TextStyle(
                      fontSize: 15.0,
                      color: Colors.white.withOpacity(0.6),
                    ),
                  ),
                  SizedBox(
                    height: 5.0,
                    width: 300,
                    child: Divider(
                      color: Colors.white.withOpacity(0.6),
                    ),
                  ),
                  Container(
                      margin: const EdgeInsets.symmetric(
                          vertical: 1.0, horizontal: 25.0),
                      width: double.infinity,
                      height: 200,
                      child: GoogleMapsWidget(
                        latitud: double.parse(
                            miUser.results[0].location.coordinates.latitude),
                        longitud: double.parse(
                            miUser.results[0].location.coordinates.longitude),
                      )),
                  SizedBox(
                    height: 5.0,
                    width: 300,
                    child: Divider(
                      color: Colors.white.withOpacity(0.6),
                    ),
                  ),
                  infoCard(
                    icon: Icons.phone,
                    text: miUser.results[0].phone,
                  ),
                  infoCard(
                    icon: Icons.people,
                    text: miUser.results[0].gender,
                  ),
                  infoCard(
                    icon: Icons.circle,
                    text: miUser.results[0].location.country,
                  ),
                ],
              ),
            ),
          );
        });
  }

  Widget infoCard({required IconData icon, required String text}) {
    return Card(
      margin: const EdgeInsets.symmetric(vertical: 0.5, horizontal: 25.0),
      child: ListTile(
        leading: Icon(
          icon,
          size: 35.0,
          color: Colors.blueGrey,
        ),
        title: Text(
          text,
          style: const TextStyle(
            fontSize: 20.0,
            color: Colors.blueAccent,
          ),
        ),
      ),
    );
  }
}
