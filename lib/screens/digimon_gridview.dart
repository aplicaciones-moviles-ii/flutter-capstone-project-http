import 'package:capstone_project/models/digimon.dart';
import 'package:capstone_project/request/digimon_http_request.dart';
import 'package:capstone_project/screens/digimon_detail.dart';
import 'package:capstone_project/widgets/digimon_cuadricula_widget.dart';
import 'package:flutter/material.dart';

class DigimonGridView extends StatefulWidget {
  String cardSetName;
  DigimonGridView({Key? key, required this.cardSetName}) : super(key: key);

  @override
  _DigimonGridViewState createState() => _DigimonGridViewState();
}

class _DigimonGridViewState extends State<DigimonGridView> {
  late Future<List<Digimon>> futureDigimons;

  @override
  void initState() {
    super.initState();
    futureDigimons =
        DigimonHttpRequest().getDigimonCardSets(widget.cardSetName);
  }

  void showDigimonDetail(String cardNumber, String imageUrl) {
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return DigimonDetail(
        cardNumber: cardNumber,
        imageUrl: imageUrl,
      );
    }));
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<Digimon>>(
      future: futureDigimons,
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return const Center(
            child: CircularProgressIndicator.adaptive(),
          );
        }
        return Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.black,
            title: const Text("Digimon Video Screen"),
          ),
          backgroundColor: Colors.black,
          body: GridView.builder(
            itemCount: snapshot.data?.length ?? 0,
            itemBuilder: (context, index) {
              List<Digimon>? digimons = snapshot.data;
              return DigimonCuadriculaWidget(
                digimon: digimons![index],
                onTap: showDigimonDetail,
              );
            },
            gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 3,
                mainAxisSpacing: 3,
                crossAxisSpacing: 3,
                childAspectRatio: 0.7),
          ),
        );
      },
    );
  }
}
