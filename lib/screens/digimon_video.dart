import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';

class DigimonVideo extends StatefulWidget {
  const DigimonVideo({Key? key}) : super(key: key);

  @override
  State<DigimonVideo> createState() => _DigimonVideoState();
}

class _DigimonVideoState extends State<DigimonVideo> {
  late VideoPlayerController _controller;

  @override
  void initState() {
    super.initState();
    _controller = VideoPlayerController.network(
        'https://www.dailymotion.com/cdn/manifest/video/x84armz.m3u8?sec=cYekrOHaQH3NMfWvoL0gHQ2cAeGBsSSqscsVYir1ZF0BqK8XJfc-tlIARZ3_V-xc4_usAv0VD6nsr-8DH79osA&dmTs=961705&dmV1st=C4919953E4AE3AAECF501A5E0F9E4DCB&progressive=1')
      ..initialize().then((_) {
        // Ensure the first frame is shown after the video is initialized, even before the play button has been pressed.
        setState(() {});
      });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        title: const Text("Digimon Video Screen"),
      ),
      backgroundColor: Colors.black,
      body: OrientationBuilder(
          builder: (context, orientation) =>
              orientation == Orientation.landscape
                  ? landscapeMode()
                  : portraitMode()),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          setState(() {
            _controller.value.isPlaying
                ? _controller.pause()
                : _controller.play();
          });
        },
        child: Icon(
          _controller.value.isPlaying ? Icons.pause : Icons.play_arrow,
        ),
      ),
    );
  }

  Row landscapeMode() {
    return Row(
      children: [
        Expanded(
          child: _controller.value.isInitialized
              ? AspectRatio(
                  aspectRatio: _controller.value.aspectRatio,
                  child: VideoPlayer(_controller),
                )
              : Container(),
        ),
      ],
    );
  }

  Column portraitMode() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center, //horizontal
      children: [
        TextTitleVideo(),
        TextDetailVideo(),
        Expanded(
          child: _controller.value.isInitialized
              ? AspectRatio(
                  aspectRatio: _controller.value.aspectRatio,
                  child: VideoPlayer(_controller),
                )
              : Container(),
        ),
      ],
    );
  }

  Text TextDetailVideo() {
    return const Text(
        "Digimon is the revolutionary game that is entertaining young and old!!",
        textAlign: TextAlign.justify,
        style: TextStyle(fontSize: 20.0, color: Colors.white));
  }

  Text TextTitleVideo() {
    return const Text(
      "DIGIMON - THE GAME",
      style: TextStyle(
          fontFamily: "IndieFlower",
          fontSize: 30.0,
          color: Colors.yellow,
          backgroundColor: Colors.black),
    );
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }
}
