import 'package:capstone_project/models/digimon.dart';
import 'package:capstone_project/request/digimon_http_request.dart';
import 'package:capstone_project/screens/digimon_detail.dart';
import 'package:capstone_project/widgets/digimon_widget.dart';
import 'package:flutter/material.dart';

class DigimonListView extends StatefulWidget {
  const DigimonListView({Key? key}) : super(key: key);

  @override
  _DigimonListViewState createState() => _DigimonListViewState();
}

class _DigimonListViewState extends State<DigimonListView> {
  late Future<List<Digimon>> futureDigimons;

  @override
  void initState() {
    super.initState();
    futureDigimons = DigimonHttpRequest().getDigimons;
  }

  void showDigimonDetail(String cardNumber, String imageUrl) {
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return DigimonDetail(
        cardNumber: cardNumber,
        imageUrl: imageUrl,
      );
    }));
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<Digimon>>(
      future: futureDigimons,
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return const Center(
            child: CircularProgressIndicator.adaptive(),
          );
        }
        return ListView.builder(
          itemCount: snapshot.data?.length ?? 0,
          itemBuilder: (context, index) {
            List<Digimon>? digimons = snapshot.data;
            return Container(
              margin: const EdgeInsets.fromLTRB(12, 12, 12, 0),
              padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
              height: 150,
              decoration: const BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(20.0)),
                boxShadow: [
                  BoxShadow(color: Colors.blue, offset: Offset(1, 1))
                ],
              ),
              child: DigimonWidget(
                digimon: digimons![index],
                onTap: showDigimonDetail,
              ),
            );
          },
        );
      },
    );
  }
}
