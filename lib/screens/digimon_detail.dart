import 'package:capstone_project/constants.dart';
import 'package:capstone_project/models/digimon.dart';
import 'package:capstone_project/request/digimon_http_request.dart';
import 'package:capstone_project/screens/digimon_gridview.dart';
import 'package:capstone_project/widgets/getting_data_widget.dart';
import 'package:flutter/material.dart';

class DigimonDetail extends StatefulWidget {
  String cardNumber = '';
  String imageUrl = '';
  DigimonDetail({Key? key, required this.cardNumber, required this.imageUrl})
      : super(key: key);

  @override
  State<DigimonDetail> createState() => _DigimonDetailState();
}

class _DigimonDetailState extends State<DigimonDetail> {
  late Future<Digimon> futureDigimon;

  @override
  void initState() {
    super.initState();
    futureDigimon = DigimonHttpRequest().getDigimon(widget.cardNumber);
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<Digimon>(
      future: futureDigimon,
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return const gettingDataWidget();
        }
        Digimon? miDigimon = snapshot.data;
        return Scaffold(
          appBar: AppBar(
            backgroundColor: miDigimon!.color,
            title: Text(
              widget.cardNumber,
              style: const TextStyle(
                fontSize: 30,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          body: Padding(
            padding: const EdgeInsets.all(15),
            child: Column(
              children: [
                Expanded(
                  flex: 2,
                  child: Row(children: [
                    Expanded(
                      flex: 2,
                      child: Column(
                        children: [
                          rowData("Name:", miDigimon.name),
                          rowData("Type:", miDigimon.type),
                          rowData("Stage", miDigimon.stage.toString()),
                          rowData("Attribute:", miDigimon.attribute),
                          TextButton(
                              onPressed: () => {
                                    Navigator.push(context,
                                        MaterialPageRoute(builder: (context) {
                                      return DigimonGridView(
                                        cardSetName: miDigimon.setName,
                                      );
                                    })),
                                  },
                              child: const Text("View Card Set"))
                        ],
                      ),
                    ),
                    Expanded(
                        flex: 1,
                        child: levelBanner(
                            miDigimon.level.toString(), miDigimon.color)),
                  ]),
                ),
                Divider(
                  color: miDigimon.color,
                  thickness: 5,
                ),
                Expanded(
                  flex: 5,
                  child: Row(
                    children: [
                      Expanded(
                        child: Image.network(
                          widget.imageUrl,
                          height: double.infinity,
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        );
      },
    );
  }

  Row rowData(String label, String data) {
    return Row(
      children: [
        Expanded(
          flex: 2,
          child: Text(
            label,
            style: digimonLabelDetailTextStyle,
          ),
        ),
        Expanded(
          flex: 5,
          child: Text(
            data,
            style: digimonContainDetailTextStyle,
          ),
        ),
      ],
    );
  }

  SizedBox levelBanner(String level, Color? colorBanner) {
    return SizedBox(
      width: double.infinity,
      height: double.infinity,
      child: Scaffold(
        backgroundColor: colorBanner,
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const Text("LEVEL",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  )),
              Text(
                level,
                style: const TextStyle(
                  color: Colors.white,
                  fontSize: 70,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
