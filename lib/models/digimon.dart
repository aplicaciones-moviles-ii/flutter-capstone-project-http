import 'dart:convert';
import 'package:flutter/material.dart';

List<Digimon> digimonsFromJson(String str) =>
    List<Digimon>.from(json.decode(str).map((x) => Digimon.fromJson(x)));

Digimon digimonFromJson(String str) =>
    List<Digimon>.from(json.decode(str).map((x) => Digimon.fromJson(x))).first;

class Digimon {
  Digimon(
      {required this.name,
      required this.type,
      required this.color,
      required this.img,
      required this.stage,
      required this.cardNumber,
      required this.attribute,
      required this.setName,
      required this.level});

  String name;
  String type;
  String img;
  String? stage;
  String cardNumber;
  String attribute;
  String setName;
  int level;
  Color? color;

  factory Digimon.fromJson(Map<String, dynamic> json) {
    return Digimon(
      name: json["name"],
      type: json["type"],
      color: colorValues.map[json["color"]] ?? Colors.black,
      img: json["image_url"] ??
          "https://images.digimoncard.io/images/cards/BO-115.jpg",
      stage: json["stage"] ?? "NO DATA",
      attribute: json["attribute"] ?? "NO DATA",
      setName: json["set_name"] ?? "NO DATA",
      level: json["level"] ?? 0,
      cardNumber: json["cardnumber"],
    );
  }
}

class EnumValues<T> {
  Map<String, T> map;
  EnumValues(this.map);
}

final colorValues = EnumValues({
  "Black": Colors.black,
  "Blue": Colors.blue,
  "Colorless": Colors.grey,
  "": Colors.black,
  "Green": Colors.green,
  "Purple": Colors.purple,
  "Red": Colors.red,
  "2  Red": Colors.redAccent,
  "White": Colors.grey,
  "Yellow": Colors.amberAccent
});
