import 'package:capstone_project/screens/user_screen.dart';
import 'package:capstone_project/screens/digimon_video.dart';
import 'package:flutter/material.dart';

const double sizeButton = 75;

class FlowMenu extends StatefulWidget {
  const FlowMenu({Key? key}) : super(key: key);

  @override
  _FlowMenuState createState() => _FlowMenuState();
}

class _FlowMenuState extends State<FlowMenu>
    with SingleTickerProviderStateMixin {
  late AnimationController controller;

  void initState() {
    super.initState();
    controller = AnimationController(
      duration: const Duration(milliseconds: 100),
      vsync: this,
    );
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Flow(
      delegate: FlowItemDelegate(controller: controller),
      children: <IconData>[
        Icons.house,
        Icons.play_arrow,
        Icons.person,
      ].map<Widget>(iconItem).toList(),
    );
  }

  SizedBox iconItem(IconData icon) => SizedBox(
        width: sizeButton,
        height: sizeButton,
        child: FloatingActionButton(
            elevation: 20,
            splashColor: Colors.blue,
            child: Icon(
              icon,
              color: Colors.white,
              size: 45,
            ),
            onPressed: () {
              if (controller.status == AnimationStatus.completed &&
                  icon == Icons.house) {
                controller.reverse();
              } else if (icon == Icons.play_arrow) {
                controller.reverse();
                Navigator.push(context, MaterialPageRoute(builder: (context) {
                  return const DigimonVideo();
                }));
              } else if (icon == Icons.person) {
                controller.reverse();
                Navigator.push(context, MaterialPageRoute(builder: (context) {
                  return const UserScreen();
                }));
              } else {
                controller.forward();
              }
            }),
      );
}

class FlowItemDelegate extends FlowDelegate {
  final Animation<double> controller;
  const FlowItemDelegate({required this.controller})
      : super(repaint: controller);

  @override
  void paintChildren(FlowPaintingContext context) {
    final size = context.size;
    final xStart = size.width - sizeButton - 20;
    final yStart = size.height - sizeButton - 20;

    for (int iconIndex = context.childCount - 1; iconIndex >= 0; iconIndex--) {
      const marging = 10;
      final childSize = context.getChildSize(iconIndex)!.width;
      final dx = (childSize + marging) * iconIndex;
      final x = xStart;
      final y = yStart - dx * controller.value;

      context.paintChild(iconIndex,
          transform: Matrix4.translationValues(
            x,
            y,
            0,
          ));
    }
  }

  @override
  bool shouldRepaint(FlowItemDelegate oldDelegate) {
    return controller != oldDelegate.controller;
  }
}
