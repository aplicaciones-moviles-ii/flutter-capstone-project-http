import 'package:capstone_project/constants.dart';
import 'package:capstone_project/models/digimon.dart';
import 'package:flutter/material.dart';

class DigimonWidget extends StatelessWidget {
  final Function onTap;
  final Digimon? digimon;

  const DigimonWidget({
    Key? key,
    required this.digimon,
    required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        onTap(digimon!.cardNumber, digimon!.img);
      },
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Expanded(
            flex: 1,
            child: Image.network(
              digimon!.img,
              width: 150,
              height: 150,
            ),
          ),
          Expanded(
            flex: 2,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Text(digimon!.name, style: digimonNameTextStyle),
                Text(digimon!.cardNumber, style: digimonNameTextStyle),
                Text(
                  digimon!.stage.toString(),
                  style: TextStyle(
                    color: digimon!.color,
                    fontSize: 25,
                    fontWeight: FontWeight.bold,
                    letterSpacing: -2,
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
