import 'package:flutter/material.dart';

class gettingDataWidget extends StatelessWidget {
  const gettingDataWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: const [
            Text("Getting data..."),
            CircularProgressIndicator.adaptive(),
          ],
        ),
      ),
    );
  }
}
