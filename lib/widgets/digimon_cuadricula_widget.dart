import 'package:capstone_project/models/digimon.dart';
import 'package:flutter/material.dart';

class DigimonCuadriculaWidget extends StatelessWidget {
  final Function onTap;
  final Digimon? digimon;

  const DigimonCuadriculaWidget({
    Key? key,
    required this.digimon,
    required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        onTap(digimon!.cardNumber, digimon!.img);
      },
      child: SizedBox(
        child: Scaffold(
          backgroundColor: digimon!.color,
          body: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(digimon!.cardNumber,
                    style: const TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                    )),
                Image.network(
                  digimon!.img,
                  height: 130,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
