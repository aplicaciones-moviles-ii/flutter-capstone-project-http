import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class GoogleMapsWidget extends StatefulWidget {
  GoogleMapsWidget({Key? key, required this.latitud, required this.longitud})
      : super(key: key);

  double longitud, latitud;

  @override
  _GoogleMapsWidgetState createState() => _GoogleMapsWidgetState();
}

class _GoogleMapsWidgetState extends State<GoogleMapsWidget> {
  @override
  Widget build(BuildContext context) {
    return GoogleMap(
      initialCameraPosition: CameraPosition(
          target: LatLng(widget.latitud, widget.longitud), zoom: 10),
      myLocationButtonEnabled: true,
    );
  }
}
