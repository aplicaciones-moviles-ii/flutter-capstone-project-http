import 'package:flutter/material.dart';

const digimonNameTextStyle = TextStyle(
  color: Colors.black,
  fontSize: 25,
  fontWeight: FontWeight.bold,
  letterSpacing: -2,
);

const digimonContainDetailTextStyle =
    TextStyle(color: Colors.black, fontSize: 15);

const digimonLabelDetailTextStyle =
    TextStyle(color: Colors.black, fontSize: 12, fontWeight: FontWeight.bold);
